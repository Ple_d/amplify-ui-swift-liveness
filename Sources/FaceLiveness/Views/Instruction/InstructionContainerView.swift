//
// Copyright Amazon.com Inc. or its affiliates.
// All Rights Reserved.
//
// SPDX-License-Identifier: Apache-2.0
//

import SwiftUI
import Combine

struct InstructionContainerView: View {
    @ObservedObject var viewModel: FaceLivenessDetectionViewModel

    var body: some View {
        switch viewModel.livenessState.state {
        case .initial:
            InstructionView(
                text: "Wait",
                backgroundColor: .white,
                textColor: .black,
                font: .title
            )
        case .initialClientInfoEventSent:
            InstructionView(
                text: "Wait",
                backgroundColor: .white,
                textColor: .black,
                font: .title
            )
        case .displayingFreshness:
            InstructionView(
                text: .challenge_instruction_hold_still,
                backgroundColor: .white,
                textColor: .black,
                font: .title
            )
            .onAppear {
                UIAccessibility.post(
                    notification: .announcement,
                    argument: NSLocalizedString(
                        "amplify_ui_liveness_challenge_instruction_hold_still",
                        bundle: .module,
                        comment: ""
                    )
                )
            }

        case .awaitingFaceInOvalMatch(.faceTooClose, _):
            InstructionView(
                text: .challenge_instruction_move_face_back,
                backgroundColor: .white,
                textColor: .black,
                font: .title
            )
            .onAppear {
                UIAccessibility.post(
                    notification: .announcement,
                    argument: NSLocalizedString(
                        "amplify_ui_liveness_challenge_instruction_move_face_back",
                        bundle: .module,
                        comment: ""
                    )
                )
            }

        case .awaitingFaceInOvalMatch(let reason, let percentage):
            InstructionView(
                text: .init(reason.rawValue),
                backgroundColor: .white,
                textColor: .black,
                font: .title
            )

            ProgressBarView(
                emptyColor: .white,
                borderColor: .hex("#AEB3B7"),
                fillColor: .green,
                percentage: percentage
            )
            .frame(width: 160, height: 8)
        case .recording(ovalDisplayed: true):
            InstructionView(
                text: .challenge_instruction_move_face_closer,
                backgroundColor: .white,
                textColor: .black,
                font: .title
            )
            .onAppear {
                UIAccessibility.post(
                    notification: .announcement,
                    argument: NSLocalizedString(
                        "amplify_ui_liveness_challenge_instruction_move_face_closer",
                        bundle: .module,
                        comment: ""
                    )
                )
            }

            ProgressBarView(
                emptyColor: .white,
                borderColor: .hex("#AEB3B7"),
                fillColor: .green,
                percentage: 0.2
            )
            .frame(width: 160, height: 8)
        case .pendingFacePreparedConfirmation(let reason):
            InstructionView(
                text: .init(reason.rawValue),
                backgroundColor: .white,
                font: .title
            )
        case .completedDisplayingFreshness:
            InstructionView(
                text: .challenge_verifying,
                backgroundColor: .white,
                font: .title
            )
            .onAppear {
                UIAccessibility.post(
                    notification: .announcement,
                    argument: NSLocalizedString(
                        "amplify_ui_liveness_challenge_verifying",
                        bundle: .module,
                        comment: ""
                    )
                )
            }
        default:
            EmptyView()
        }
    }
}
