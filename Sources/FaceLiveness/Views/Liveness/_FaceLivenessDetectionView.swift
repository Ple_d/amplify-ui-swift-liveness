//
// Copyright Amazon.com Inc. or its affiliates.
// All Rights Reserved.
//
// SPDX-License-Identifier: Apache-2.0
//

import SwiftUI

struct _FaceLivenessDetectionView<VideoView: View>: View {
    let videoView: VideoView
    @ObservedObject var viewModel: FaceLivenessDetectionViewModel
    @Binding var displayResultsView: Bool

    init(
        viewModel: FaceLivenessDetectionViewModel,
        @ViewBuilder videoView: @escaping () -> VideoView
    ) {
        self.viewModel = viewModel
        self.videoView = videoView()

        self._displayResultsView = .init(
            get: { viewModel.livenessState.state == .completed },
            set: { _ in }
        )
    }

    var body: some View {
        ZStack {
            Color.white
            ZStack {
                videoView
            }
            
            VStack {
                HStack(alignment: .top) {
                    CloseButton(
                        action: viewModel.closeButtonAction
                    )
                    
                    Spacer()
                }
                .padding()
                
                Spacer()
            }
            .padding([.leading, .trailing])
            .frame(maxWidth: .infinity)
            
            VStack {
                Spacer()
                
                InstructionContainerView(
                    viewModel: viewModel
                )
                .padding(.bottom, 20)
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .edgesIgnoringSafeArea(.horizontal)
    }
}
