//
// Copyright Amazon.com Inc. or its affiliates.
// All Rights Reserved.
//
// SPDX-License-Identifier: Apache-2.0
//

import Foundation
import UIKit

class OvalView: UIView {
    let ovalFrame: CGRect

    init(frame: CGRect, ovalFrame: CGRect) {
        self.ovalFrame = CGRect(x: ovalFrame.minX, y: ovalFrame.minY + 20, width: ovalFrame.width, height: ovalFrame.height - 50)
        super.init(frame: frame)
        backgroundColor = .clear
    }

    override func draw(_ rect: CGRect) {
        let mask = UIBezierPath(rect: bounds)
//        let oval = UIBezierPath(ovalIn: ovalFrame)
        let oval = UIBezierPath(roundedRect: ovalFrame, cornerRadius: ovalFrame.width / 2)
        mask.append(oval.reversing())

        UIColor.white.withAlphaComponent(1).setFill()
        mask.fill()

        UIColor.clear.setFill()
        UIColor.white.setStroke()
        oval.lineWidth = 8
        oval.stroke()
    }

    required init?(coder: NSCoder) { nil }
    
    func greenFrame() {
        UIColor.green.setStroke()
    }
}
