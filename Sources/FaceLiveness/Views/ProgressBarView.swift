//
// Copyright Amazon.com Inc. or its affiliates.
// All Rights Reserved.
//
// SPDX-License-Identifier: Apache-2.0
//

import SwiftUI

struct ProgressBarView: View {
    let emptyColor: Color
    let borderColor: Color
    let fillColor: Color
    let percentage: Double

    var body: some View {
        GeometryReader { proxy in
            ZStack(alignment: .leading) {
                Rectangle()
                    .border(borderColor, width: 1)
                    .cornerRadius(8, corners: .allCorners)
                    .frame(
                        width: proxy.size.width,
                        height: proxy.size.height
                    )
                    .foregroundColor(emptyColor)

                Group {
                    Rectangle()
                        .cornerRadius(8, corners: .allCorners)
                        .frame(
                            width: min(percentage, 1) * proxy.size.width,
                            height: proxy.size.height
                        )
                        .foregroundColor(fillColor)
                }
                .frame(width: proxy.size.width, alignment: .center)
            }
        }
    }
}

struct ProgressBarView_PriveiewView: PreviewProvider {
    static var previews: some View {
        VStack {
            ProgressBarView(emptyColor: .white,
                            borderColor: .black,
                            fillColor: .red,
                            percentage: 1)
                .frame(width: 160, height: 8, alignment: .top)
                .padding()
            
            ProgressBarView(emptyColor: .white,
                            borderColor: .black,
                            fillColor: .red,
                            percentage: 0.2)
            .frame(width: 160, height: 8, alignment: .top)
            .padding()
        }
    }
}
